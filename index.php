<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Giải phương trình bậc nhất</title>
    <link rel="stylesheet" href="app.css">
</head>
<body>
    <?php 
        $result = $a = $b = '';
        if (isset($_POST['calculate'])) {
        
            $a = isset($_POST['a']) ? (float)trim($_POST['a']) : '';
            $b = isset($_POST['b']) ? (float)trim($_POST['b']) : '';
    
   
            if ($a == 0) {
                $result = '<span class="text-danger">Số a phải nhập khác 0</span> ';
            } else {
                $result = 'X = '. - ($b) / $a;
            }
        } 

    ?>
    <div class="main">
        <h4>Giải phương trình bậc nhất</h4>
        <div class="content">
        <form method="post" action="index.php">
        <input type="number" required class="content_number" name="a" value="<?php echo $a;?>" placeholder="Nhập a..."/> X
        +
        <input type="number" required class="content_number" name="b" value="<?php echo $b ?>" placeholder="Nhập b..."/> = 0

        <input type="submit" name="calculate" value="Tính"/>
    </form>
    <?php echo '<div style="margin-top:5px">'. $result. '</div>' ?>
        </div>
    </div>
</body>
</html>